require('rootpath')();
const express = require('express');
const server = express();

const cors = require('cors');
const bodyParser = require('body-parser');
const errorHandler = require('helpers/error-handler');

server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
server.use(cors());

server.use('/users', require('./users/user.controller'));
server.use(errorHandler);

server.get("/",(req,res) => {
    res.send("Welcome");
});

server.listen(6069, function () {
    console.log('Server listening on port ' + 6069);
})